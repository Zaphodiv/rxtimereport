/** Record time between state changes on a 433MHz reciever.
 *  (Code designed for arduino uno/mega - uses pin 2 change interrupt)
 *  
 *  Instructions:
 *  1. Connect the reciever to pin 2 on an arduino (tested on Uno or Mega)
 *  2. Press and hold transmitter button to send a message contiuously 
 *  3. Open serial monitor (115200 baud)
 *  4. Wait until serial monitor displays some data, then releae the transmitter button.
 *  
 *  The output should be 
 *     a dashed line (indicating the pause between messages
 *     a number > 10000 - this is the guard time (in uS) on my transmitter it is somewhere around 13000 but can vary a bit.
 *     a set of pairs of numbers representing mark and space time (in uS)
 *     (probably) a single short number representing the mark before end of message.
 *     
 * IMPORTANT: the timings are approximate and have a typical resolution of 4uS.
 * 
 * Use the timing values to work out the best values for:
 *  1. Preamble time (mark/space)
 *  2. Logic 1 (mark/space)
 *  3. Logic 2 (mark/space)
 *  4. End of message (mark)
 *  5. Guard time between messages (space)
 *  
 * Partial example
 *----------------
 * 12984       # guard time 
 *  1524   804 # Preamble 
 *   756   292 # Data bits: (on my system 40 in total)
 *   752   288 # Long then short -> Logic 1
 *   752   292 
 *   760   280 
 *   248   792 # Short then long -> Logic 0
 *   252   792 
 *   252   788 
 *   768   284 
 *     
  * ```
 *   ____________           ________     ....  ________      _
 * _/            \_________/____\\\\\___ .... /____\\\\\____/ \_______________________________
 * |                       |                                |                                |
 * | Preamble ~1550/800us  | Data (40 bits) 1: 750/200us  * |  Guard time ~13300us           | 
 * |                       |                0: 250/800us  * |                                |
 * ```
 * measured times, roughly. Seems to be some leeway in these figures
 * [modified timings https://github.com/CrashOverride85/collar ]
 */
#include <avr/interrupt.h>

const    long maxEvents      = 240;

volatile bool hasData        = false; // true when we have recorded maxEvents
volatile bool lastState      = false;
volatile int  lastTriggerUs  = 0L;
volatile long lastEventUs    = 0L;
volatile long eventsCounted  = 0L;

const    byte RX_PIN         = 2; // ardunio pin

const uint16_t stateBit = 0x8000;

// bottom 15 bit is event time top bit is event value (true = 0x8000, false = 0x0000)
volatile uint16_t  eventTimes[maxEvents + 2];
volatile bool      eventValues[maxEvents + 2];
volatile bool started = false;

/** Interrupt service routine to record state changes on `RX_PIN`.
 *  Storing time between events in a buffer until maxEvents reached.
 */
void isr ()
{
  long usNow = micros();
  if (lastTriggerUs == 0)
  {
    lastTriggerUs = usNow;
  }
  lastEventUs = usNow - lastTriggerUs;
  lastState = digitalRead(RX_PIN); // don't like this


  if (started || (lastEventUs > 10000) )
  {
    lastTriggerUs = usNow;
    started = true;    
    eventTimes[eventsCounted]  = lastEventUs;
    eventValues[eventsCounted] = lastState;
    if (eventsCounted < maxEvents)
      eventsCounted++;
    else
      hasData = true;
  }
} 


void setup() {
  Serial.begin(115200);
  pinMode(RX_PIN, INPUT);
  digitalWrite(RX_PIN, HIGH);
  memset((void*)eventTimes, 0, maxEvents * sizeof(eventTimes[0])); 
  memset((void*)eventValues, 0, maxEvents * sizeof(eventValues[0])); 
  delay(1000);
  Serial.println("Waiting for timer event");
  lastTriggerUs = micros();

  lastState = digitalRead(RX_PIN);
  // Start listening
  attachInterrupt(digitalPinToInterrupt(RX_PIN), isr, CHANGE);  // attach interrupt handler
}

void loop() {
  char buffer[10]; // should only need 8+1
  static bool trigger = false;
  if (hasData)
  {
    // stop listening
    detachInterrupt(digitalPinToInterrupt(2));
    hasData = false;
    for (long i = 0; i < maxEvents; i++)
    {
      bool eventState = eventValues[i];
      int eventTime =   eventTimes[i];
      if (eventTime > 10000)
      {
        Serial.println("\n----------------");
        trigger = true;
      }
      if (trigger)
      {
        sprintf(buffer, "%5d ", eventTime);
        Serial.print(buffer);
        if (eventState)
          Serial.println();
      }
    }
    Serial.println();
  }
}
