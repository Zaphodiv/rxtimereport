# RxTimeReport

A simple program for extracting timing data for protocol analysis of 433MHz transmitter.

## Hardware

* Arduino (for example Uno or Mega)
* 433MHz receiver module [MX-05V](https://hobbycomponents.com/wired-wireless/615-433mhz-wireless-receiver-module-mx-05)

| Rx Connection | Arduino connection |
| ------------- | ------------------ |
| Vcc           | 5V                 |
| GND           | GND                |
| DATA          | Digital pin 2      |

## Software

See `RxTimeReport.ino` for sketch

Record time between state changes on a 433MHz reciever.
(Code designed for arduino uno/mega - uses pin 2 change interrupt)

Instructions:
1. Connect the reciever to pin 2 on an arduino (tested on Uno or Mega)
2. Press and hold transmitter button to send a message contiuously 
3. Open serial monitor (115200 baud)
4. Wait until serial monitor displays some data, then releae the transmitter button.


The output should be 
- a dashed line (indicating the pause between messages
-  a number > 10000 - this is the guard time (in uS) on my transmitter it is somewhere around 13000 but can vary a bit.
-  a set of pairs of numbers representing mark and space time (in uS)
- (probably) a single short number representing the mark before end of message.

**IMPORTANT**: the timings are approximate and have a typical resolution of 4uS.

Use the timing values to work out the best values for:
1. Preamble time (mark/space)
2. Logic low/0 (mark/space)
3. Logic high/1 (mark/space)
4. End of message (mark)
5. Guard time between messages (space)

### Partial example output
```
----------------
12984       # guard time 
 1524   804 # Preamble 
  756   292 # Data bits: (on my system 40 in total)
  752   288 # Long then short -> Logic 1
  752   292 
  760   280 
  248   792 # Short then long -> Logic 0
  252   792 
  252   788 
  768   284 
```

### Timing diagram derived from example
```
  ____________           ________     ....  ________      _
_/            \_________/____\\\\\___ .... /____\\\\\____/ \_______________________________
|                       |                                |                                |
| Preamble ~1550/800us  | Data (40 bits) 1: 750/200us  * |  Guard time ~13300us           | 
|                       |                0: 250/800us  * |                                |
```
Measured times, roughly. Seems to be some leeway in these figures.
**See also** https://github.com/CrashOverride85/collar

